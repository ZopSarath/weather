import React from 'react'
import { configure } from 'enzyme'
import WeatherDisplay from '../WeatherDisplay'
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer'
configure({ adapter: new Adapter() })

describe('WeatherCard', () => {
  it('should render WeatherCard correctly', () => {
    const tree = renderer.create(<WeatherDisplay />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
