import React from 'react'
import { configure } from 'enzyme'
import { OriginalCard } from '../OriginalCard'
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer'
configure({ adapter: new Adapter() })

describe('IconCard', () => {
  it('should render IconCard correctly', () => {
    const tree = renderer.create(<OriginalCard />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
