import axios from 'axios'
import TaskTwo from '../task2'
import renderer from 'react-test-renderer'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
configure({ adapter: new Adapter() })

describe('Task2():', () => {
  it('check Initial states', () => {
    const wrapper = shallow(<TaskTwo />)
    expect(wrapper.state().city).toBe('Bengaluru')
    expect(wrapper.state().status).toBe(true)
    expect(wrapper.state().isLoading).toBe(false)
    expect(wrapper.state().submitClicked).toBe(false)
  })
  it('check state being changed after for componentDidMount called', () => {
    const wrapper = shallow(<TaskTwo />)
    const tasktwo = new TaskTwo()
    tasktwo.componentDidMount()
    expect(wrapper.state().status).toBe(true)
  })
  it('should render TaskTwo correctly', () => {
    const tree = renderer.create(<TaskTwo />).toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('should test axios', () => {
    axios.get = jest.fn()
    axios.get.mockResolvedValueOnce({ status: 200 })
  })
})
