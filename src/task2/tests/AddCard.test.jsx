import React from 'react'
import { configure } from 'enzyme'
import { AddCard } from '../AddCard'
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer'
configure({ adapter: new Adapter() })

describe('App', () => {
  it('should render app correctly', () => {
    const tree = renderer.create(<AddCard />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
