import React from 'react'
import { shallow, configure } from 'enzyme'
import WeatherForm from '../WeatherForm'
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer'
configure({ adapter: new Adapter() })

describe('WeatherForm', () => {
  test('should check if handleSubmit is being invoked', () => {
    const handleSubmit = jest.fn()
    const wrapper = shallow(<WeatherForm handleSubmit={handleSubmit} />)
    const form = wrapper.find('form')
    expect(form.exists()).toBe(true) // check if form exists
    form.invoke('onSubmit')()
    expect(handleSubmit).toHaveBeenCalledTimes(1)
  })
  test('should render WeatherForm correctly', () => {
    const tree = renderer.create(<WeatherForm />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
