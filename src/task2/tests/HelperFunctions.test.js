import { timeConvert, dayConvert, dateConvert } from '../HelperFunctions'

describe('HelperFunctions', () => {
  test.each([
    [18, '6 PM'],
    [10, '10 AM'],
    [22, '10 PM'],
  ])('should test timeConvert', (hour, expected) => {
    const response = timeConvert(hour)
    expect(response).toBe(expected)
  })

  test.each([
    [0, 'Sun'],
    [1, 'Mon'],
    [2, 'Tue'],
  ])('should test dayConvert', (day, expected) => {
    const response = dayConvert(day)
    expect(response).toBe(expected)
  })

  test.each([
    ['0 21', 'Jan 21'],
    ['3 18', 'Apr 18'],
    ['11 1', 'Dec 1'],
  ])('should test dateConvert', (date, expected) => {
    const response = dateConvert(date)
    expect(response).toBe(expected)
  })
})
