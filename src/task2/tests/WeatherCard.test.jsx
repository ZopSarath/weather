import React from 'react'
import { configure } from 'enzyme'
import WeatherCard from '../WeatherCard'
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer'
configure({ adapter: new Adapter() })

describe('WeatherCard', () => {
  it('should render WeatherCard correctly', () => {
    const tree = renderer.create(<WeatherCard />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
