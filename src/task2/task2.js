import React, { Component } from 'react'
import './styles/task2.css'
import WeatherDisplay from './WeatherDisplay'
import WeatherForm from './WeatherForm'
import axios from 'axios'

class TaskTwo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      city: 'Bengaluru',
      temp: [],
      humidity: [],
      sky: [],
      dateTime: [],
      icon: [],
      descp: [],
      windSpeed: [],
      status: true,
      isLoading: false,
      submitClicked: false,
      temp_max_min: {},
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.getWeatherData = this.getWeatherData.bind(this)
  }

  apikey = '1b1f3ba7c9c6cd45e39d115e78efacfb'

  getWeatherData() {
    let list = []
    let temp_responses = []
    let humidity_responses = []
    let sky_status = []
    let dateTimes = []
    let icons = []
    let descp = []
    let windSpeed = []

    axios
      .get(
        `https://api.openweathermap.org/data/2.5/forecast?q=${this.state.city}&appid=${this.apikey}`
      )
      .then((res) => {
        list = res.data.list.slice(0, 40).filter((item, index) => {
          return index % 8 === 0
        })
        temp_responses = list.map((item) =>
          (item.main.temp - 273.15).toFixed(1)
        )
        humidity_responses = list.map((item) => item.main.humidity)
        sky_status = list.map((item) => item.weather[0].main)
        dateTimes = list.map((item) => item.dt_txt)
        icons = list.map((item) => item.weather[0].icon)
        descp = list.map((item) => item.weather[0].description)
        windSpeed = list.map((item) => item.wind.speed.toFixed(1))
        let temp_max_min = res.data.list.slice(0, 40).reduce((acc, day) => {
          const date = day.dt_txt.split(' ')[0]
          if (acc[date]) {
            acc[date].temp_max = Math.max(acc[date].temp_max, day.main.temp_max)
            acc[date].temp_min = Math.min(acc[date].temp_min, day.main.temp_min)
          } else {
            acc[date] = {
              temp_max: day.main.temp_max,
              temp_min: day.main.temp_min,
            }
          }
          return acc
        }, {})
        temp_max_min = Object.values(temp_max_min).slice(0, 5)
        this.setState({
          status: true,
          temp: temp_responses,
          humidity: humidity_responses,
          sky: sky_status,
          dateTime: dateTimes,
          icon: icons,
          descp: descp,
          windSpeed: windSpeed,
          isLoading: false,
          temp_max_min: temp_max_min,
        })
      })
      .catch(() => {
        this.setState({ status: false, isLoading: false })
      })
  }

  handleSubmit = (event) => {
    const data = new FormData(event.target)
    this.setState({ city: data.get('city'), submitClicked: true })
    event.preventDefault()
  }

  componentDidMount() {
    this.getWeatherData()
  }

  componentDidUpdate() {
    if (this.state.submitClicked === true) {
      this.setState({ submitClicked: false, isLoading: true })
      this.getWeatherData()
    }
  }

  render() {
    return (
      <section className="task2-container">
        <section className="app-name">Know Your Weather</section>
        <WeatherForm city={this.state.city} handleSubmit={this.handleSubmit} />
        <WeatherDisplay {...this.state} />
        <section className="current-date-time">{Date().toString()}</section>
      </section>
    )
  }
}

export default TaskTwo
