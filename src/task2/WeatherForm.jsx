import React, { Component } from 'react'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './styles/WeatherForm.css'

class WeatherForm extends Component {
  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <section className="form-container">
          <section className="current-city">Currently in</section>
          <section className="input-container">
            <input
              type="text"
              id="city"
              name="city"
              placeholder={this.props.city}
              className="input"
              autocomplete="off"
            />
            <button className="button">
              <FontAwesomeIcon icon={faSearch} size="1x" />
            </button>
          </section>
        </section>
      </form>
    )
  }
}

export default WeatherForm
