export const timeConvert = (time) => {
  let decider = ''
  decider = time < 12 ? ' AM' : ' PM' // Set AM/PM
  time = time % 12 || 12 // Adjust hours
  return time + decider // return adjusted time or original string
}

export const dayConvert = (day) => {
  let days = {
    0: 'Sun',
    1: 'Mon',
    2: 'Tue',
    3: 'Wed',
    4: 'Thu',
    5: 'Fri',
    6: 'Sat',
  }
  return days[day]
}

export const dateConvert = (date) => {
  let months = {
    0: 'Jan',
    1: 'Feb',
    2: 'Mar',
    3: 'Apr',
    4: 'May',
    5: 'Jun',
    6: 'Jul',
    7: 'Aug',
    8: 'Sep',
    9: 'Oct',
    10: 'Nov',
    11: 'Dec',
  }
  date = date.split(' ')
  return months[date[0]] + ' ' + date[1]
}
