import React from 'react'
import './styles/OriginalCard.css'

export const OriginalCard = (props) => {
  return (
    <section className="original-card">
      <section className="date-time">
        <section className="day">{props.day}</section>
        <section>{props.date}</section>
        <section>{props.time}</section>
      </section>
      <section>
        <section className="temperature">{props.temp}&#176;</section>
        Humidity: {props.humidity}
      </section>
    </section>
  )
}
