import React from 'react'
import './styles/AddCard.css'

export const AddCard = (props) => {
  return (
    <section className="new-card">
      <section className="temp-max">
        &#8593;
        {(props.temp_max_min
          ? props.temp_max_min.temp_max - 273.15
          : 0
        ).toFixed(1)}
        &#176;
      </section>

      <section className="temp-min">
        &#8595;
        {(props.temp_max_min
          ? props.temp_max_min.temp_min - 273.15
          : 0
        ).toFixed(1)}
        &#176;
      </section>

      <section className="wind-box">
        <section className="wind-speed">{props.windSpeed}</section>
        <section className="kmph">km/h</section>
      </section>
    </section>
  )
}
