import React, { Component } from 'react'
import './styles/WeatherDisplay.css'
import WeatherCard from './WeatherCard'
import { dayConvert, dateConvert, timeConvert } from './HelperFunctions'

class WeatherDisplay extends Component {
  getWeatherCard(index, addString) {
    let classString = 'display-card '
    let day = dayConvert(new Date(this.props.dateTime[index]).getDay())
    let date = dateConvert(
      new Date(this.props.dateTime[index]).getMonth() +
        ' ' +
        new Date(this.props.dateTime[index]).getDate()
    )

    let time = timeConvert(new Date(this.props.dateTime[index]).getHours())
    return (
      <WeatherCard
        classString={classString}
        addString={addString}
        day={day}
        date={date}
        time={time}
        temp={this.props.temp[index]}
        temp_max_min={this.props.temp_max_min[index]}
        humidity={this.props.humidity[index]}
        windSpeed={this.props.windSpeed[index]}
        icon={this.props.icon[index]}
        descp={this.props.descp[index]}
      />
    )
  }
  checkSky(index) {
    switch (this.props.sky[index]) {
      case 'Clouds':
        return this.getWeatherCard(index, 'gray-blur')

      case 'Clear':
        return this.getWeatherCard(index, 'sky-blue-blur')

      case 'Snow':
        return this.getWeatherCard(index, 'white-blur')

      case 'Rain':
        return this.getWeatherCard(index, 'purple-blur')

      case 'Drizzle':
        return this.getWeatherCard(index, 'gray-blur')

      case 'Thunderstorm':
        return this.getWeatherCard(index, 'purple-blur')

      default:
        return this.getWeatherCard(index, ' ')
    }
  }

  render() {
    if (this.props.isLoading) {
      return (
        <section className="display-container">
          <section className="loader"></section>
        </section>
      )
    } else if (!this.props.isLoading && this.props.status) {
      return (
        <section className="display-container">
          {this.checkSky(0)}
          {this.checkSky(1)}
          {this.checkSky(2)}
          {this.checkSky(3)}
          {this.checkSky(4)}
        </section>
      )
    } else if (!this.props.isLoading && !this.props.status) {
      return (
        <section className="display-container">
          <section className="error">
            <section className="error-msg">Uh Oh! City Doesn't Exist</section>
          </section>
        </section>
      )
    }
  }
}

export default WeatherDisplay
