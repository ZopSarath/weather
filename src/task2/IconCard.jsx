import React from 'react'
import './styles/IconCard.css'

export const IconCard = (props) => {
  return (
    <section className="icon-descp">
      <img
        src={'http://openweathermap.org/img/wn/' + props.icon + '.png'}
        alt="icon"
      />
      <section>{props.descp}</section>
    </section>
  )
}
