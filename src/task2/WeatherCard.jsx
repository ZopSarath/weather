import React, { Component } from 'react'
import './styles/WeatherCard.css'
import { OriginalCard } from './OriginalCard'
import { IconCard } from './IconCard'
import { AddCard } from './AddCard'

class WeatherCard extends Component {
  constructor() {
    super()
    this.weatherCardType = this.weatherCardType.bind(this)
  }
  weatherCardType() {
    return (
      <section className={this.props.classString + ' ' + this.props.addString}>
        <OriginalCard {...this.props} />
        <IconCard {...this.props} />
        <AddCard {...this.props} />
      </section>
    )
  }
  render() {
    return <section>{this.weatherCardType()}</section>
  }
}

export default WeatherCard
