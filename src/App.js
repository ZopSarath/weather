import React, { Component } from 'react'
import './App.css'
import {BrowserRouter} from 'react-router-dom'
import {Routes} from './Routes'
import {NavBar} from './NavBar'

class App extends Component { 
  render() {
    return (
      <React.Fragment>
        <BrowserRouter>
          <NavBar/>
          <Routes/>
      </BrowserRouter>
      </React.Fragment>
    )
  }
}

export default App
