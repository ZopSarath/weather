import React from 'react'
import { Link } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Nav} from 'react-bootstrap';
import { CodeSlash } from 'react-bootstrap-icons';
import './App.css'

export const NavBar = () => {
    return(  <Navbar bg="dark" variant="dark"  className="Navbar">
    <CodeSlash className="cyan"/>
    <Navbar.Brand><h className="cyan">React-Tasks:</h></Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link><Link to="/" className="link">Home</Link></Nav.Link> 
      <Nav.Link><Link to="/task1" className="link">Task1</Link></Nav.Link>
      <Nav.Link><Link to="/task2" className="link">Task2 </Link></Nav.Link> 
    </Nav>
  </Navbar>);
   
};