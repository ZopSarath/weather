import React from 'react'
import { shallow,  configure } from 'enzyme'
import TaskOne from './task1'
import Adapter from 'enzyme-adapter-react-16'
configure({ adapter: new Adapter() })

describe("Task1", () => {
    test("should check for the initial state value of counter",()=>{
        const wrapper = shallow(
            <TaskOne/>
        )
       expect(wrapper.state().value).toBe(0)
    })

    test.each([
        ['.forest-green', 1],
        ['.golden-rod', 0],
        ['.crimson', -1]
      ])('should change the state accordingly when the button is clicked', (className, expected) => {
        const wrapper = shallow(
            <TaskOne/>
        )  
        const button = wrapper.find(className)
        expect(button.exists()).toBe(true) // check if button exists
        button.invoke('onClick')()
        expect(wrapper.state().value).toBe(expected) // check if state is changed
      }); 
})