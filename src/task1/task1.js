import React, { Component } from 'react'
import { faPlus, faMinus, faSync } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './task1.css'
class TaskOne extends Component {
  state = {
    value: 0,
  }

  handleIncrement = () => {
    const value = this.state.value + 1
    this.setState({ value: value })
  }

  handleDecrement = () => {
    const value = this.state.value - 1
    this.setState({ value: value })
  }

  handleReset = () => {
    const value = 0
    this.setState({ value: value })
  }
  render() {
    return (
      <section className="task1-container">
        <div className="counter-display">
          <span className="number">{this.state.value}</span>
        </div>
        <section className="button-section">
          <button onClick={this.handleIncrement} className="forest-green">
            <FontAwesomeIcon icon={faPlus} size="4x" />
          </button>
          <button onClick={this.handleReset} className="golden-rod">
            <FontAwesomeIcon icon={faSync} size="4x" />
          </button>
          <button onClick={this.handleDecrement} className="crimson">
            <FontAwesomeIcon icon={faMinus} size="4x" />
          </button>
        </section>
      </section>
    )
  }
}

export default TaskOne
