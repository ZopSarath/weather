import React from 'react'
import { Route, Switch } from 'react-router-dom'
import TaskOne from './task1/task1'
import TaskTwo from './task2/task2'

export const Routes = () => {
  return (
    <div>
      <Switch>
        <Route path="/task1" exact component={TaskOne} />
        <Route path="/task2" exact component={TaskTwo} />
      </Switch>
    </div>
  )
}
